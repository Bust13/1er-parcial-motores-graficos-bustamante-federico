using UnityEngine;

public class CorrienteDeAire : MonoBehaviour
{
    public float fuerzaEmpuje = 10f; // La fuerza con la que empujar� al jugador.
    public float tiempoEntreEmpujes = 5f; // El tiempo en segundos entre cada empuje.
    private float tiempoTranscurrido = 0f;
    private bool puedeEmpujar = true;
    private GameObject jugador;
    public ParticleSystem particleSystem;
    private void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Jugador");
        particleSystem.Stop();
    }

    private void Update()
    {
        tiempoTranscurrido += Time.deltaTime;

        if (tiempoTranscurrido >= tiempoEntreEmpujes)
        {
            tiempoTranscurrido = 0f;
            puedeEmpujar = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == jugador && puedeEmpujar)
        {
            Vector3 direccion = transform.forward;
            Rigidbody rbJugador = jugador.GetComponent<Rigidbody>();
            rbJugador.AddForce(direccion * fuerzaEmpuje, ForceMode.Impulse);
            puedeEmpujar = false;
            particleSystem.Play();
        }
    }
}
