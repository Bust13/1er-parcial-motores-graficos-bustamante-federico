using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoZona : MonoBehaviour
{
    public GameObject enemigozona;
    Animation Animado;
    public bool intruso;

    public GameObject Player;
    private void Start()
    {
        Animado = enemigozona.GetComponent<Animation>();
        intruso = false;
        Animado["walk_ani_vor"].wrapMode = WrapMode.Loop;
        Animado["warte_pose"].wrapMode = WrapMode.Loop;
        Animado["walk_left"].wrapMode = WrapMode.Loop;
        Animado["walk_right"].wrapMode = WrapMode.Loop;
        Animado["Attack"].wrapMode = WrapMode.Loop;
        Animado["walk_ani_back"].wrapMode= WrapMode.Loop;
      
        Animado["walk_ani_vor"].layer = 1;
        Animado["warte_pose"].layer = 0;
        Animado["walk_left"].layer = 3;
        Animado["walk_right"].layer = 4;
        Animado["Attack"].layer = 5;
        Animado["walk_ani_back"].layer = 2;
       
        Animado.Play("warte_pose");

    }
    private void Update()
    {
      if(intruso)
        {
            Vector3 posicion = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z);
            transform.LookAt(posicion);
            transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
            Animado.Play("walk_ani_vor");
            
        }
        else
        {
            Animado.Stop("walk_ani_vor");
            Animado.CrossFade("warte_pose");
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            intruso = true;

        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag( "Jugador"))
        {
           intruso = false;

        }
    }
}
