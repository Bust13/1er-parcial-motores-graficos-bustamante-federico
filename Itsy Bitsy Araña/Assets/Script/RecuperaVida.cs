using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecuperaVida : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            GameManager.Instance.RecuperarVidas();
            Destroy(this.gameObject);
        }
    }
}
