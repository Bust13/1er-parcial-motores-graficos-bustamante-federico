using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscupitajoArcade : MonoBehaviour
{
    public GameObject[] puntos;
    public float velocidadDePlataforma = 2;
    int puntosIndex = 0;
    private void Update()
    {
        MoverEnemigo();
    }
    void MoverEnemigo()
    {

        if (Vector3.Distance(transform.position, puntos[puntosIndex].transform.position) < 0.1f)
        {

            puntosIndex++;
            if (puntosIndex >= puntos.Length)
            {
                puntosIndex = 0;

            }

        }
        transform.position = Vector3.MoveTowards(transform.position, puntos[puntosIndex].transform.position, velocidadDePlataforma * Time.deltaTime);

    }
    private void OnTriggerEnter(Collider other)
    {
       if(other.CompareTag("Jugador"))
        {
            GameManager.Instance.Perder();
        }
    }
}
