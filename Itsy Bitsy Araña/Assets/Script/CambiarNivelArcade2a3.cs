using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarNivelArcade2a3 : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            GameManager.Instance.PasarNivelArcade2();
        }
    }
}
