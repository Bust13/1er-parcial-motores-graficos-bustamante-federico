using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDispara : MonoBehaviour
{
 
    public GameObject enemigozona;
   
    public bool intruso;
    private Transform posicionJugador;
    public GameObject balaEnemigo;
    public Transform BalaSpawn;
    public float velovidadBala = 50;
    public GameObject Player;
    public float tiroRate = 0.5f;

    public float tiempoTiro = 0;
    private void Start()
    {
        
        intruso = false;
        posicionJugador = FindObjectOfType<SpiderControl>().transform;
       

    }
    private void Update()
    {
        if (intruso)
        {
            
            if (Time.time > tiempoTiro)
            {
                Vector3 posicion = posicionJugador.position - transform.position;
                GameObject nuevaBala;
                nuevaBala = Instantiate(balaEnemigo, BalaSpawn.position, BalaSpawn.rotation);
                tiempoTiro = Time.time + tiroRate;
                nuevaBala.GetComponent<Rigidbody>().AddForce( posicion* velovidadBala, ForceMode.Force);
                Invoke("Disparar", 3);
            }

        }
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            intruso = true;

        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            intruso = false;

        }
    }
}


