using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarNivel3a4 : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            GameManager.Instance.PasarNivelArcade3();
        }
    }
}
