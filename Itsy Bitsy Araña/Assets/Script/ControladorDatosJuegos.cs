using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;

public class ControladorDatosJuegos : MonoBehaviour
{
    public GameObject jugador;
    
    public string archivoDeGuardado;
    DatosJuego datosJuego = new DatosJuego();
  
    private void Awake()
    {

        archivoDeGuardado = Application.dataPath + "/datosJuego.json";
        jugador = GameObject.FindGameObjectWithTag("Jugador");
        
        
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CargarDatos();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
           GuardarDatos();
        }
    }
   
    public void CargarDatos()
    {
        if (File.Exists(archivoDeGuardado))
        {
            string contenido= File.ReadAllText(archivoDeGuardado);
            datosJuego= JsonUtility.FromJson<DatosJuego>(contenido);
           
            Debug.Log("Posicion de Jugador: " + datosJuego.posicion);
            jugador.transform.position= datosJuego.posicion;
            jugador.GetComponent<CantidadDevida>().CantidadDeVida = datosJuego.vida;
            
        }
        else
        {
            Debug.Log("El archivo No existe");
        }
    }
    private void GuardarDatos()
    {
        DatosJuego nuevosDatos = new DatosJuego()
        {
            posicion = jugador.transform.position,
            vida = jugador.GetComponent<CantidadDevida>().CantidadDeVida,
       
        };
        string cadenaJson= JsonUtility.ToJson(nuevosDatos);
        File.WriteAllText(archivoDeGuardado, cadenaJson);
        Debug.Log("Archivo Guardado");
    }
    
}
