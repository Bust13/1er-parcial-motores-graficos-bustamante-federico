using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TuberiasManager : MonoBehaviour
{
    public GameObject gameObject;
    private void Start()
    {
        StartCoroutine(CrearTubos());
    }
    IEnumerator CrearTubos()
    {
        while (true)
        {
            int yPosicion = Random.Range(-3, 3);
            GameObject nuevoTubo =Instantiate(gameObject, new Vector2 (10, yPosicion),Quaternion.identity);
            nuevoTubo.name = "Tuberias";
            yield return new WaitForSeconds(3.0f);
        }
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void JugarFlappy()
    {
        SceneManager.LoadScene("FlappySpider");
    }
    public void Salir()
    {
        Application.Quit();
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.M))
        {
            Menu();
        }
    }
}
