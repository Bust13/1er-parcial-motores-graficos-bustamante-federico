using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float velocidadMovimiento;
    public Material material;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    private void Update()
    {
        float offset = velocidadMovimiento * Time.deltaTime;
        Vector2 currentOffset = material.mainTextureOffset;
        currentOffset.x += offset;
        material.mainTextureOffset = currentOffset;
    }
}