using UnityEngine;

public class ControlMirarCamara : MonoBehaviour
{
    public Vector2 sens;
    public Transform camera;
    public float minAngle;
    public float maxAngle;

    private float currentRotation = 0f;

    private void Update()
    {
        float hor = Input.GetAxis("Mouse X");

        if (hor != 0)
        {
            currentRotation += hor * sens.x;
            currentRotation = Mathf.Clamp(currentRotation, -30f, 20f);
            transform.localEulerAngles = new Vector3(0f, currentRotation, 0f);
        }
        float ver = Input.GetAxis("Mouse Y");

        if (ver != 0)
        {
            float angle = (camera.localEulerAngles.x - ver * sens.y) % 360;
            if (angle > 180) { angle -= 360; }
            angle = Mathf.Clamp(angle, 5, 20);
            camera.localEulerAngles = Vector3.right * angle;

        }

    }
}
