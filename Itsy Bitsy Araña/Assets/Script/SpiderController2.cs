using UnityEngine;

public class SpiderController2 : MonoBehaviour
{
    public float forwardSpeed;
    public float deathDelay = 2f;
   
    private int clickCount = 0;
    private bool isMovingForward = false;
    private bool isAlive = true;
    private float timeSinceLastClick = 0f;
    private void Start()
    {
        

    }
    private void Update()
    {
        if (isAlive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        clickCount++;

                        if (!isMovingForward)
                        {
                            isMovingForward = true;
                            MoveCharacter(forwardSpeed);
                        }

                        timeSinceLastClick = 0f;
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                clickCount--;

                if (clickCount <= 0)
                {
                    isMovingForward = false;
                    MoveCharacter(0f);
                }
            }

            if (clickCount <= 0)
            {
                timeSinceLastClick += Time.deltaTime;

                if (timeSinceLastClick >= deathDelay)
                {
                    Die();
                }
            }
        }
    }

    private void MoveCharacter(float speed)
    {
        Vector3 movement = transform.forward * speed * Time.deltaTime;
        transform.position += movement;
    }
    private void Die()
    {
        isAlive = false;
        
        GameManager.Instance.Perder();
        
    }
}
