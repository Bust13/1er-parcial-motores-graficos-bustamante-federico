using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class SpiderControl : MonoBehaviour
{
    public float rotationSpeed = 3f;
    public GameObject spider;
   
    private Rigidbody rb;
    Animation anim;
    public float speed = 2f;
    public int maximoDeSaltos = 2;
    public int salto = 1;
    public float fuerza = 15f;
    public bool detectaPiso;
    public AudioClip audioSalto;
    public AudioClip audioPasos;
    private Vector3 movementDirection = Vector3.zero;
    public float gravedadPersonalizada=3f;

    void Start()
    {
        Cursor.visible = false; 
        Cursor.lockState = CursorLockMode.Locked; 
        anim = spider.GetComponent<Animation>();
        rb = GetComponent<Rigidbody>();
        anim["Jump"].wrapMode = WrapMode.Once;
        anim["walk_ani_vor"].wrapMode = WrapMode.Loop;
        anim["walk_ani_back"].wrapMode = WrapMode.Loop;
        anim["walk_right"].wrapMode = WrapMode.Loop;
        anim["walk_left"].wrapMode = WrapMode.Loop;
        anim["warte_pose"].wrapMode = WrapMode.Loop;

        //Separar en capas
        anim["Jump"].layer = 1;
        anim["walk_ani_vor"].layer = 2;
        anim["walk_ani_back"].layer = 3;
        anim["walk_right"].layer = 4;
        anim["walk_left"].layer = 5;
        anim["warte_pose"].layer = 0;

        anim.Play("warte_pose");
    }
    private void FixedUpdate()
    {
        if (!detectaPiso)
        {
            // Aplica una fuerza de gravedad personalizada para hacer que el objeto caiga m�s lentamente
            rb.AddForce(Vector3.down * gravedadPersonalizada, ForceMode.Acceleration);
        }
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true; 
            Cursor.lockState = CursorLockMode.None; 
        }
        Movement();
        Move();
        Vector3 Piso = transform.TransformDirection(Vector3.down);
        if ((Input.GetKeyDown(KeyCode.Space) && (salto > 0)))
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;
            anim.Play("Jump");
            AudioManager.Instance.ReproducirSonido(audioSalto);
        }

        
    }
    private void Movement()
    {
        float moveHorizontal = 0f;
        float moveVertical = 0f;
        #region MovVertical
        if (Input.GetKey(KeyCode.W))
        {
            moveVertical = 1f;
            anim.Play("walk_ani_vor");
            
        }
        else
        if (Input.GetKey(KeyCode.S))
        {
            moveVertical = -1f;
            anim.Play("walk_ani_back");
           
        }
        else
        {
           
            anim.Stop("walk_ani_vor");
            anim.Stop("walk_ani_back");
           
        }

        #endregion
        #region MovHorizontal
        if (Input.GetKey(KeyCode.D))
        {
            moveHorizontal = 1f;
            anim.Play("walk_right");
          
        }
        else if (Input.GetKey(KeyCode.A))
        {
            moveHorizontal = -1f;
            anim.Play("walk_left");
           
        }
        else
        {
          
            anim.Stop("walk_left");
            anim.Stop("walk_right");
        }


        #endregion
        movementDirection = new Vector3(moveHorizontal, 0f, moveVertical);
        
    }
    void Move()
    {
        rb.velocity = movementDirection * speed;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = true;
            salto = maximoDeSaltos;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = false;
        }
    }
   
}