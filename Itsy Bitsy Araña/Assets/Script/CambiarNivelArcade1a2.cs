using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarNivelArcade1a2 : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            GameManager.Instance.PasarNivelArcade1();
        }
    }
}
