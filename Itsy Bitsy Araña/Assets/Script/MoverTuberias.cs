using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverTuberias : MonoBehaviour
{
    int speed = 3;

    private void Update()
    {
        this.transform.Translate(-speed* Time.deltaTime,0,0);
    }
}
