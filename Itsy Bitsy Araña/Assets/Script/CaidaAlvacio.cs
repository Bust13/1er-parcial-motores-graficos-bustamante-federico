using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaidaAlvacio : MonoBehaviour
{
    public ParticleSystem particleSystem;
    private void Start()
    {
        particleSystem.Stop();
    }
    private void OnCollisionEnter(Collision other)
    {
        

        if (other.gameObject.CompareTag("Jugador"))
        {
            GameManager.Instance.Perder();
            particleSystem.Play();
        }
    }
}
