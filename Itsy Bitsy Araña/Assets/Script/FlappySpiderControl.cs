using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappySpiderControl : MonoBehaviour
{
    
    private Rigidbody rb;
    public float jumpForce = 13;
    private float initialSize;
    private int i = 0;
    public float tiempoInicial = 0f;
    private float tiempoActual;
    private bool contadorActivo = false;
    public HUD hud;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.W))
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Perder();

        }
    }
    public void Perder()
    {
        Destroy(rb);
        hud.ActivarGameOver();
       

    }
}


