using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public GameObject[] vidas;
    public GameObject AranaPierde;
    
    public void DesactivarVidas(int indice)
    {
        vidas[indice].SetActive(false);
    }
    public void ActivarVidas(int indice)
    {
        vidas[indice].SetActive(true);
    }

    public void ActivarGameOver()
    {
        AranaPierde.SetActive(true);
    }
}
