using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using Unity.VisualScripting;

public class GameManager : MonoBehaviour

{
    public ParticleSystem particleSystem;
    public static GameManager Instance { get; private set; }
    public HUD hud;
    public GameObject Spider;
    public CantidadDevida devida;
    private void Start()
    {
        particleSystem.Stop();
    }
    private void Awake()
    {

        if (Instance == null)
        {

            Instance = this;
        }

    }
   
    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.M))
        {
            Menu();
        }
    }
    public void PerderVidas()
    {
        devida.CantidadDeVida -= 1;
        if (devida.CantidadDeVida == 0)
        {
            hud.ActivarGameOver();
            particleSystem.Play();
            Destroy(GameObject.FindWithTag("Enemigo"));
        }
        hud.DesactivarVidas(devida.CantidadDeVida);

    }
    public void RecuperarVidas()
    {if (devida.CantidadDeVida<3)
        hud.ActivarVidas(devida.CantidadDeVida);
        devida.CantidadDeVida += 1;
    }

        public void Perder()
    {
        
            hud.ActivarGameOver();
             particleSystem.Play();

    }
    public void JugarFlappy()
    {
        SceneManager.LoadScene("FlappySpider");
    }
    public void JugarArcade()
    {
        SceneManager.LoadScene(3);
    }
    public void PasarNivelArcade1()
    {
        SceneManager.LoadScene("Arcade2");
    }
    public void PasarNivelArcade2()
    {
        SceneManager.LoadScene("Arcade3");
    }
    public void PasarNivelArcade3()
    {
        SceneManager.LoadScene("Arcade4");
    }

    public void Jugar()
    {
        SceneManager.LoadScene("Nivel1");
    }
    public void Ganaste()
    {
        SceneManager.LoadScene("Ganaste");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void SelecciondeNivelesArcade()
    {
        SceneManager.LoadScene("SeleccionDeNivelesArcade");
    }
    public void Salir()
    {
        Application.Quit();
    }
}