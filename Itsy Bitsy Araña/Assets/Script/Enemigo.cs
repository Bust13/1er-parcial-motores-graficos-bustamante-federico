using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
           GameManager.Instance.PerderVidas();
        }
        if (other.gameObject.CompareTag("Bala"))
        {
           Destroy(gameObject);
        }
    }
}